# coding=utf-8

# Pretty-printers for DUNE.

# Copyright (C) 2011 Jan-Hendrik Peters

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# XXX One file for each Dune module?

import gdb
import re
import pprint


class DuneFVectorPrinter(object):
    "prints a Dune::FieldVector in a pretty way"

    def __init__(self, val):
        self.val = val

    def to_string(self):
        regex = re.match("^Dune::FieldVector<\w+,\s+(?P<length>\d+)>$", self.val.type.tag)
        # parse the length via regexp instead of using val.type.template_argument(int index)
        # see: http://users.dune-project.org/issues/88
        f_vec_length = regex.group('length')
        f_vec_type = self.val.type.template_argument(0)
        print "Dune::FieldVector of type %s and length %s:" %(f_vec_type, f_vec_length)
        return decode_dune_field_vector(self.val)

    def display_hint(self):
	"classify this printer as an array-like printer"
        return 'array'

def decode_dune_field_vector(val):
    "decodes a Dune::FieldVector object and returns a pretty string in form of {1,2,...}"
    # determine the field vector's length, can't use val.type.template_argument(0) because 
    # of a bug in GDB 7.0; see: http://users.dune-project.org/issues/88
    regex = re.match("^Dune::FieldVector<\w+,\s+(?P<length>\d+)>$", val.type.tag)
    length = regex.group('length')
    if length == '1':
        return str(val['_data'])
    # ['a'] is only needed if the vector is longer than 1
    return str(val['_data']['a'])

def dune_common_FieldVector(val):
    "loopkup function for Dune::FieldVector"
    lookup_tag = val.type.tag
    if lookup_tag == None:
        return None
    regex = re.match("^Dune::FieldVector<\w+,\s+\d+>$", lookup_tag)
    if regex:
        return DuneFVectorPrinter(val)
    return None



class DuneFMatrixPrinter(object):
    "prints a Dune::FieldMatrix in a pretty way"

    def __init__(self, val):
        self.val = val

    def display_hint(self):
	"classify this printer as an array-like printer"
        return 'array'

    def to_string(self):
        # parse the dimension via regexp instead of using val.type.template_argument(int index)
        # see: http://users.dune-project.org/issues/88
        regex = re.match("^Dune::FieldMatrix<\w+,\s+(?P<m>\d+),\s+(?P<n>\d+)>$", self.val.type.tag)
        f_mat_m = regex.group('m')
        f_mat_n = regex.group('n')
        f_mat_type = self.val.type.template_argument(0)
        mat_string = ''
        print "A %sx%s Dune::FieldMatrix of type %s:" %(f_mat_m, f_mat_n, f_mat_type)

        # special case: 1x1 matrix
        if f_mat_m == "1" and f_mat_n == "1":
            return self.val['_data']['_data']

        # iterate over the matrix line by line
        for i in range(0, int(f_mat_m)):
            # every line of the matrix, is a Dune::Fieldvector
            mat_string += str(decode_dune_field_vector(self.val['_data']['a'][i]))+"\n"
        return mat_string

def dune_common_FieldMatrix(val):
    "loopkup function for Dune::FieldMatrix"
    lookup_tag = val.type.tag
    if lookup_tag == None:
        return None
    regex = re.match("^Dune::FieldMatrix<\w+,\s+\d+,\s+\d+>$", lookup_tag)
    if regex:
        return DuneFMatrixPrinter(val)
    return None



class DuneBitSetVectorPrinter(object):
    "prints a Dune::BitSetVector in a pretty way"
    set_size = 0

    # private iterator, needed by the children method
    class _iterator:
        def __init__ (self, start, finish):
            self.set_size = set_size
            self.item   = start['_M_p']
            self.so     = start['_M_offset']
            self.finish = finish['_M_p']
            self.fo     = finish['_M_offset']
            item_type = self.item.dereference().type
            # get item's size in bits
            self.item_size = 8 * item_type.sizeof
            # counter needed for the iterator
            self.count = 0

        def __iter__(self):
            return self

        def next(self):
            tmp_set = []
            # create lists from the std::vector at size of the specified set
            for i in range(0, int(set_size)):
                self.count = self.count + 1
                if self.item == self.finish and self.so >= self.fo:
                    raise StopIteration
                curr_object = self.item.dereference()
                # check if curr_object (the object in the array we are
                # currently pointig at) is referenced
                if curr_object & (1 << self.so):
                    curr_value = 1
                else:
                    curr_value = 0
                tmp_set.append(curr_value)
                self.so = self.so + 1
                if self.so >= self.item_size:
                    self.item = self.item + 1
                    self.so = 0
            # return a tuple of a counter and one list with the size of a set
            return ('[%d]' % self.count, str(tmp_set))

    def __init__(self, val):
        self.val = val

    def display_hint(self):
	"classify this printer as an array-like printer"
        return 'array'

    def children(self):
        return self._iterator(self.val['_M_impl']['_M_start'],
                              self.val['_M_impl']['_M_finish'])

    def to_string(self):
        # parse the size via regexp instead of using val.type.template_argument(int index)
        # see: http://users.dune-project.org/issues/88
        regex = re.match("^Dune::BitSetVector<(?P<set_size>\d+),\s+std::allocator<\w+> >$",str(self.val.type))
        # the set's size is needed to partition the vector
        global set_size
        set_size = regex.group('set_size')
        start = self.val['_M_impl']['_M_start']['_M_p']
        finish = self.val['_M_impl']['_M_finish']['_M_p']
        so = self.val['_M_impl']['_M_start']['_M_offset']
        fo = self.val['_M_impl']['_M_finish']['_M_offset']
        itype = start.dereference().type
        # not sure if a bool has the same size on every processor platform, so better get it with sizeof()
        bl = 8 * itype.sizeof
        length = (bl - so) + bl * ((finish - start) - 1) + fo
        return ("Dune::BitSetVector with %d sets; each of size %s" % (int(length)/int(set_size),  set_size))

def dune_common_BitSetVector(val):
    "loopkup function for Dune::BitSetVector"
    # NOTE: The BitSetVector class has no val.type.tag; therefore I use this regexp to determine what I need
    regex = re.match("^Dune::BitSetVector<\d+,\s+std::allocator<(?P<alloc_type>\w+)> >$", str(val.type))
    if regex:
        # NOTE: There is no use case for a BSV with a non bool allocator. Thus we don't support it.
        # We instead warn the user and return "None" which let's GDB print the raw object.
        # This would not be possible we would register the printer via gdb.printing.RegexpCollectionPrettyPrinter
        if regex.group('alloc_type') != 'bool':
            print "This pretty-printer does not support Dune::BitSetVector objects with a non bool std::allocator."
            return None
        return DuneBitSetVectorPrinter(val)
    return None


# register the lookup functions for the given objfile or globally if it's not given
def appender(obj):
    if obj == None:
        obj = gdb
    # register the Dune::FieldVector printer
    obj.pretty_printers.append(dune_common_FieldVector)
    # register the Dune::FieldMatrix printer
    obj.pretty_printers.append(dune_common_FieldMatrix)
    # register the Dune::BitSetVector printer
    obj.pretty_printers.append(dune_common_BitSetVector)
























